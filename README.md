## Description

Below are instructions to complete a basic technical task as part of an interiew process. For bonus task, head over to [Bonus branch](https://gitlab.com/tkostus/aid-tech-task/tree/bonus). The goal is to create & publish a basic React app (or reuse an example app) and deploy it to Kubernetes using Helm.

## Prerequisites

- Kubernetes cluster (I'm using Azure Kubernetes Service - AKS)
- Docker
- kubectl
- Azure CLI

Provision of K8S cluster and installation of Docker, kubectl & Azure CLI are outside of the scope for this exercise.

## Installation of Helm (using Windows)

Helm is a separate service, not available in barebone Kubernetes cluster. It comprises of Helm Client - a CLI application, and Tiller - a service running in the cluster.

#### Install Helm Client

Easiest with Chocolatey (through _elevated command prompt_):

```
choco install kubernetes-helm
```

#### Enable tiller to manage K8S cluster in Azure

For RBAC-enabled clusters. For non-RBAC-enabled clusters, skip this step. More detailed instructions can be found [here](https://docs.microsoft.com/en-us/azure/aks/kubernetes-helm).

- Create a service account:

```
kubectl apply -f helm-rbac.yaml
```

#### Install helm-tiller

For non-RBAC-enabled clusters, drop the `--service-account tiller` part.

```
helm init --service-account tiller
```

## Building & pushing docker image

To build a docker image of the example app (inspired by [this](https://medium.com/@shakyShane/lets-talk-about-docker-artifacts-27454560384f) post), type:

```
docker build . -t your_username/aid-tech-task
```

Test the image locally:

```
docker run -p 8080:80 your_username/aid-tech-task
```

Push the image to Docker Hub (remember to _docker login_ first):

```
docker image push your_username/aid-tech-task
```

## Deploying to Kubernetes manually (optional step)

Deploy application:

```
kubectl apply -f ./helmchart/templates/deployment.yaml
```

Deploy service:

```
kubectl create -f ./helmchart/templates/service.yaml
```

## Deploying to Kubernetes using Helm

Couldn't be simpler:

```
helm install helmchart
```

## Accessing via DNS

Example application running in Kubernetes and should be accessible via DNS:

[http://task.effco.net](http://task.effco.net)

## Helpful commands

Below is a list of helpful commands which could be useful in completing the above task.

#### Getting the credentials for your cluster from Azure

```
az aks get-credentials --resource-group [[ResourceGroupName]] --name [[ClusterName]]
```

#### Connecting to K8S dashboard in Azure

- Create dashboard account role binding (for RBAC enabled cluster)

```
kubectl create clusterrolebinding kubernetes-dashboard --clusterrole=cluster-admin --serviceaccount=kube-system:kubernetes-dashboard
```

- Create a proxy

```
az aks browse --resource-group [[ResourceGroupName]] --name [[ClusterName]]
```
