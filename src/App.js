import React, { Component } from 'react'
import { Spring, config } from 'react-spring'
import './App.css'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Spring
            config={config.slow}
            delay={1000}
            from={{ opacity: 0 }}
            to={{ opacity: 1 }}
          >
            {props => (
              <div style={props}>
                <p>{`Hello Stranger!`}</p>
              </div>
            )}
          </Spring>
        </header>
      </div>
    )
  }
}

export default App
